package com.machine.learning.iris.techieiris.service;

import com.machine.learning.iris.service.ClasificadorEspecieService;
import com.machine.learning.iris.techieiris.TechieIrisApplicationTests;
import java.math.BigDecimal;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class ClasificadorEspecieServiceTest extends TechieIrisApplicationTests{

    @Autowired
    private ClasificadorEspecieService predictorService;

    @Test
    public void clasificarEspecie_conDatosDeEspecieSetosa_retornaEspecieSetosa() {

        BigDecimal longitudPetaloSetosa = new BigDecimal(1.0);
        BigDecimal anchoPetaloSetosa = new BigDecimal(0.5);

        String prediccionEspecie = predictorService.clasificarEspecie(longitudPetaloSetosa, anchoPetaloSetosa);

        assertThat(prediccionEspecie).isNotNull();
        assertThat(prediccionEspecie).isEqualTo("Iris-setosa");

    }
    
    @Test
    public void clasificarEspecie_conDatosDeEspecieVirginica_retornaEspecieVirginica() {

        BigDecimal longitudPetaloSetosa = new BigDecimal(7.0);
        BigDecimal anchoPetaloSetosa = new BigDecimal(2.5);

        String prediccionEspecie = predictorService.clasificarEspecie(longitudPetaloSetosa, anchoPetaloSetosa);

        assertThat(prediccionEspecie).isNotNull();
        assertThat(prediccionEspecie).isEqualTo("Iris-virginica");

    }
    
    @Test
    public void clasificarEspecie_conDatosDeEspecieVersicolor_retornaEspecieVersicolor() {

        BigDecimal longitudPetaloSetosa = new BigDecimal(3.0);
        BigDecimal anchoPetaloSetosa = new BigDecimal(1.0);

        String prediccionEspecie = predictorService.clasificarEspecie(longitudPetaloSetosa, anchoPetaloSetosa);

        assertThat(prediccionEspecie).isNotNull();
        assertThat(prediccionEspecie).isEqualTo("Iris-versicolor");

    }

}
