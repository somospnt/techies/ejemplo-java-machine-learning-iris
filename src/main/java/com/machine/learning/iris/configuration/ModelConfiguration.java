package com.machine.learning.iris.configuration;

import java.io.IOException;
import java.io.InputStream;
import javax.xml.bind.JAXBException;
import org.dmg.pmml.PMML;
import org.jpmml.evaluator.Evaluator;
import org.jpmml.evaluator.ModelEvaluatorBuilder;
import org.jpmml.model.PMMLUtil;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.xml.sax.SAXException;

@Configuration
public class ModelConfiguration {

    @Bean
    public Evaluator modelEvaluator() throws SAXException, JAXBException, IOException {
        InputStream resource = new ClassPathResource("KNN.pmml").getInputStream();
        PMML pmmlArchivo = PMMLUtil.unmarshal(resource);
        ModelEvaluatorBuilder modelEvaluatorBuilder = new ModelEvaluatorBuilder(pmmlArchivo);
        Evaluator evaluator = (Evaluator) modelEvaluatorBuilder.build();
        return evaluator;
    }

}
