package com.machine.learning.iris;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TechieIrisApplication {

	public static void main(String[] args) {
		SpringApplication.run(TechieIrisApplication.class, args);
	}

}
