package com.machine.learning.iris.controller.rest;

import com.machine.learning.iris.service.ClasificadorEspecieService;
import java.math.BigDecimal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ClasificadorEspecieRestController {

    @Autowired
    private ClasificadorEspecieService predictorService;

    @GetMapping("/clasificarEspecie")
    public String predecir(@RequestParam BigDecimal longitudPetalo, @RequestParam BigDecimal anchoPetalo) {
        System.out.println("Se obtuvo resultado:" + predictorService.clasificarEspecie(longitudPetalo, anchoPetalo));
        
        return predictorService.clasificarEspecie(longitudPetalo, anchoPetalo);
    }

}
