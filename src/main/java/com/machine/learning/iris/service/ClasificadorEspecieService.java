package com.machine.learning.iris.service;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;
import org.dmg.pmml.FieldName;
import org.jpmml.evaluator.Evaluator;
import org.jpmml.evaluator.EvaluatorUtil;
import org.jpmml.evaluator.FieldValue;
import org.jpmml.evaluator.TypeInfos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClasificadorEspecieService {

    @Autowired
    private Evaluator modelEvaluator;

    public String clasificarEspecie(BigDecimal longitudPetalo, BigDecimal anchoPetalo) {
        Map<FieldName, FieldValue> argumentos = new LinkedHashMap<>();

        FieldName fieldNameLongitud = FieldName.create("PetalLengthCm");
        FieldName fieldNameAncho = FieldName.create("PetalWidthCm");
        FieldValue fieldValueLongitud = FieldValue.create(TypeInfos.CATEGORICAL_FLOAT, longitudPetalo.floatValue());
        FieldValue fieldValueAncho = FieldValue.create(TypeInfos.CATEGORICAL_FLOAT, anchoPetalo.floatValue());
        
        argumentos.put(fieldNameLongitud, fieldValueLongitud);
        argumentos.put(fieldNameAncho, fieldValueAncho);

        Map<String, String> resultRecord = (Map<String, String>) EvaluatorUtil.decodeAll(modelEvaluator.evaluate(argumentos));
        return resultRecord.get("Species");
    }
}
