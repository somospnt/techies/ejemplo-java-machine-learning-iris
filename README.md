# Techi Iris Hola Mundo en Java

Este proyecto mostrara como exponer un modelo clasificador de Machine Learning como servicio REST en JAVA.

Para ello usaremos un modelo entrenado en Python exportado como archivo PMML.

## Dependecias

Usaremos la librerias [JPMML-Evaluator]([https://github.com/jpmml/jpmml-evaluator](https://github.com/jpmml/jpmml-evaluator))  y [JPMML-Model]([https://github.com/jpmml/jpmml-model](https://github.com/jpmml/jpmml-model)) para construir el modelo en JAVA a partir del archivo .pmml generado previamente.
